<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?= $sidebarSection == 'category' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCategory" aria-expanded="true" aria-controls="collapseCategory">
          <i class="fas fa-fw fa-cog"></i>
          <span>Category</span>
        </a>
        <div id="collapseCategory" class="collapse <?= $sidebarSection == 'category' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="manage-category.php">Manage Category</a>
            <a class="collapse-item <?= $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="add-category.php">Add Category</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?= $sidebarSection == 'customer' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCustomer" aria-expanded="true" aria-controls="collapseCustomer">
          <i class="fas fa-fw fa-cog"></i>
          <span>Customers</span>
        </a>
        <div id="collapseCustomer" class="collapse <?= $sidebarSection == 'customer' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="manage-customer.php">Manage Customers</a>
            <a class="collapse-item <?= $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="add-customer.php">Add Customers</a>
          </div>
        </div>
      </li>

      <li class="nav-item <?= $sidebarSection == 'employees' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEmployees" aria-expanded="true" aria-controls="collapseEmployees">
          <i class="fas fa-fw fa-cog"></i>
          <span>Employees</span>
        </a>
        <div id="collapseEmployees" class="collapse <?= $sidebarSection == 'employees' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSection == 'manage' ? 'active' : ''; ?>" href="manage-employee.php">Manage Employees</a>
            <a class="collapse-item <?= $sidebarSection == 'add' ? 'active' : ''; ?>" href="add-employee.php">Add Employees</a>
          </div>
        </div>
      </li>

      <li class="nav-item <?= $sidebarSection == 'products' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProducts" aria-expanded="true" aria-controls="collapseProducts">
          <i class="fas fa-fw fa-cog"></i>
          <span>Products</span>
        </a>
        <div id="collapseProducts" class="collapse <?= $sidebarSection == 'products' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSection == 'manage' ? 'active' : ''; ?>" href="#">Manage Products</a>
            <a class="collapse-item <?= $sidebarSection == 'manage' ? 'active' : ''; ?>" href="#">Check Current Activity</a>
            <a class="collapse-item <?= $sidebarSection == 'add' ? 'active' : ''; ?>" href="#">Add Product</a>
          </div>
        </div>
      </li>
      
      <li class="nav-item <?= $sidebarSection == 'suppliers' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCustomer" aria-expanded="true" aria-controls="collapseCustomer">
          <i class="fas fa-fw fa-cog"></i>
          <span>Suppliers</span>
        </a>
        <div id="collapseCustomer" class="collapse <?= $sidebarSection == 'suppliers' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="manage-supplier.php">Manage Suppliers</a>
            <a class="collapse-item <?= $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="add-supplier.php">Add Supplier</a>
          </div>
        </div>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Reports</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.html">Purchase History</a>
            <a class="collapse-item" href="register.html">Sales Report</a>
            <a class="collapse-item" href="forgot-password.html">P &amp; L Statement</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>