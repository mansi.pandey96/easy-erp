<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Add Employee";
$sidebarSection = "employees";
$sidebarSubSection = "add";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  Session::unsetSession('old');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require_once __DIR__ . "/../includes/head-section.php";
  ?>

  <!--PLACE TO ADD YOUR CUSTOM CSS-->

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Employees</h1>
            <a href="<?= BASEPAGES; ?>manage-customer.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-list-ul fa-sm text-white-75"></i> Manage Employees
            </a>
          </div>

          <div class="row">

            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Add Employee</h6>
                </div>
                <div class="card-body">
                  <div class="col-md-12">

                    <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add_employee">
                      <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                      <!--FORM GROUP-->
                      <p class="h4 title">Employee's details</p>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="first_name">Enter First Name</label>
                                <input  type="text" 
                                        name="first_name" 
                                        id="first_name" 
                                        class="form-control <?= $errors!='' && $errors->has('first_name') ? 'error' : '';?>"
                                        placeholder = "Enter First Name"
                                        value="<?=$old != '' && isset($old['first_name']) ?$old['first_name']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('first_name'))
                                  {
                                    echo "<span class='error'>{$errors->first('first_name')}</span>";
                                  }
                                ?>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="last_name">Enter Last Name</label>
                                <input  type="text" 
                                        name="last_name" 
                                        id="last_name" 
                                        class="form-control <?= $errors!='' && $errors->has('last_name') ? 'error' : '';?>"
                                        placeholder = "Enter Last Name"
                                        value="<?=$old != '' && isset($old['last_name']) ?$old['last_name']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('last_name'))
                                  {
                                    echo "<span class='error'>{$errors->first('last_name')}</span>";
                                  }
                                ?>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="email">Enter your email</label>
                                <input  type="text" 
                                        name="email" 
                                        id="email" 
                                        class="form-control <?= $errors!='' && $errors->has('email') ? 'error' : '';?>"
                                        placeholder = "Enter your email"
                                        value="<?=$old != '' && isset($old['email']) ?$old['email']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('email'))
                                  {
                                    echo "<span class='error'>{$errors->first('email')}</span>";
                                  }
                                ?>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="phone">Enter phone number</label>
                                <input  type="text" 
                                        name="phone" 
                                        id="phone" 
                                        class="form-control <?= $errors!='' && $errors->has('phone') ? 'error' : '';?>"
                                        placeholder = "Enter phone number"
                                        value="<?=$old != '' && isset($old['phone']) ?$old['phone']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('phone'))
                                  {
                                    echo "<span class='error'>{$errors->first('phone')}</span>";
                                  }
                                ?>
                              </div>
                          </div>
                      </div>
                      
                      <p class="h4 title">Address details</p>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="block_no">Enter block number</label>
                                <input  type="text" 
                                        name="block_no" 
                                        id="block_no" 
                                        class="form-control <?= $errors!='' && $errors->has('block_no') ? 'error' : '';?>"
                                        placeholder = "Enter block number"
                                        value="<?=$old != '' && isset($old['block_no']) ?$old['block_no']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('block_no'))
                                  {
                                    echo "<span class='error'>{$errors->first('block_no')}</span>";
                                  }
                                ?>
                             </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="street">Enter street</label>
                                <input  type="text" 
                                        name="street" 
                                        id="street" 
                                        class="form-control <?= $errors!='' && $errors->has('street') ? 'error' : '';?>"
                                        placeholder = "Enter street"
                                        value="<?=$old != '' && isset($old['street']) ?$old['street']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('street'))
                                  {
                                    echo "<span class='error'>{$errors->first('street')}</span>";
                                  }
                                ?>
                             </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="city">Enter city</label>
                                <input  type="text" 
                                        name="city" 
                                        id="city" 
                                        class="form-control <?= $errors!='' && $errors->has('city') ? 'error' : '';?>"
                                        placeholder = "Enter city"
                                        value="<?=$old != '' && isset($old['city']) ?$old['city']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('city'))
                                  {
                                    echo "<span class='error'>{$errors->first('city')}</span>";
                                  }
                                ?>
                             </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="pincode	">Enter pincode</label>
                                <input  type="text" 
                                        name="pincode" 
                                        id="pincode" 
                                        class="form-control <?= $errors!='' && $errors->has('pincode') ? 'error' : '';?>"
                                        placeholder = "Enter pincode"
                                        value="<?=$old != '' && isset($old['pincode']) ?$old['pincode']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('pincode'))
                                  {
                                    echo "<span class='error'>{$errors->first('pincode')}</span>";
                                  }
                                ?>
                             </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="state">Enter state</label>
                                <input  type="text" 
                                        name="state" 
                                        id="state" 
                                        class="form-control <?= $errors!='' && $errors->has('state') ? 'error' : '';?>"
                                        placeholder = "Enter state"
                                        value="<?=$old != '' && isset($old['state']) ?$old['state']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('state'))
                                  {
                                    echo "<span class='error'>{$errors->first('state')}</span>";
                                  }
                                ?>
                             </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                <label for="country">Enter country</label>
                                <input  type="text" 
                                        name="country" 
                                        id="country" 
                                        class="form-control <?= $errors!='' && $errors->has('country') ? 'error' : '';?>"
                                        placeholder = "Enter country"
                                        value="<?=$old != '' && isset($old['country']) ?$old['country']: '';?>"/>
                                <?php
                                  if($errors!="" && $errors->has('country'))
                                  {
                                    echo "<span class='error'>{$errors->first('country')}</span>";
                                  }
                                ?>
                             </div>
                          </div>
                      </div>
                      <!--/FORM GROUP-->
                      <button type="submit" class="btn btn-primary" name="add_employee" value="addEmployee"><i class="fa fa-check"></i> Submit</button>
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>
  
  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__ . "/../includes/page-level/category/add-category-scripts.php");?>
</body>

</html>