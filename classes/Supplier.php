<?php

require_once __DIR__."/../helper/requirements.php";

class Supplier{
    private $table = "suppliers";
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    
    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            'first_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 30,
            ],
            'last_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 30,
            ],
            'email' => [
                'required' =>true,
                'unique' => $this->table
            ],
            'phone' => [
                'required' => true,
                'minlength' => 10,
                'maxlength' => 10,
                'unique' => $this->table
            ],
            'gst_no' =>[
                'required' => true,
                'maxlength' => 15,
                'unique' => $this->table
            ],
            'block_no' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 5
            ],
            'street' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 35
            ],
            'city' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 35
            ],
            'pincode' => [
                'required' => true,
                'minlength' => 7,
                'maxlength' => 7
            ],
            'state' => [
                'required' => true,
                'minlength' => 1,
                'maxlength' => 40
            ],
            'country' => [
                'required' => true,
                'minlength' => 3,
                'maxlength' =>15
            ]

        ]);
    }
    /**
     * This function is responsible to accept the data from the Routing and add it to the Database.
     */
    public function addSupplier($data)
    {
        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            //Validation was successful
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();
                
                $data_to_be_inserted = ['first_name'=> $data["first_name"],'last_name' => $data['last_name'],'email' => $data['email'],'phone'=>$data['phone'],'gst_no' =>$data['gst_no']];
                $supplier_id = $this->database->insert($this->table, $data_to_be_inserted);
                
                $data_to_be_inserted = ['block_no' => $data['block_no'],'street' => $data['street'], 'city' => $data['city'], 'pincode' => $data['pincode'], 'state' => $data['state'], 'country' => $data['country']];
                $address_id = $this->database->insert("address",$data_to_be_inserted);
                
                $data_to_be_inserted = ['address_id' => $address_id, 'supplier_id' => $supplier_id];
                $address_customer_id = $this->database->insert("address_supplier",$data_to_be_inserted);
                
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }
}